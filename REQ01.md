#Carbusiness
##REQ01 - Levantamiento de requerimientos
El usuario tiene una flotilla de vehiculos, la cual esta clasificada en dos tipos segun su objetivo: vehiculos para taxistas(bajo costo) y vehiculos para uso privado(costo variable)

###Renta a Taxistas
* El usuario renta vehiculos a taxistas 500 pesos diarios
* La renta de los mismos es por tiempo indefinido.
* El cobro se realiza usualmente semanal, pero para personas de poca confianza se hace cada dos dias.
* El usuario asume los gastos de reparacion de estos vehiculos.
* El usuario requiere los datos personales de los taxistas al alquilarlos, registra la fecha de inicio de alquiler y el monto que dejo en deposito.
* El usuario registra los cambios de aceite de los vehiculos para saber cuando hay que volverlo a cambiar, la fecha de vencimiento del seguro y los gastos de mantenimiento de cada vehiculo separado por  pieza o parte que necesita revision para luego saber si necesita cambiar el vehiculo o si necesita una revision especial.

###Renta privada
* El usuario necesita un contrato por cada cliente en caso de cualquier fechoria realizada utilizando el vehiculo alquilado.
* El usuario registra los datos personales del cliente (nombre, cedula, dirección, ocupación, pasaporte, residente/extranjero, etc.).
* El usuario registra el tiempo definido de la renta, el deposito que dejo el cliente.
* Cada vehiculo tiene su propio costo diario de alquiler dependiendo del tipo y las condiciones.

###Generales
* El usuario tiene un empleado que tambien puede hacer todas sus funciones.
* El usuario necesita poder vizualizar los ingresos y egresos producidos mensualmente, la ganancia total.
* El usuario puede registrar pagos diarios, semanales, mensuales.
* El usuario necesita vizualizar los pagos que se han hecho y los que se deben