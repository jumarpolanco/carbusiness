'use strict';

angular.module('mean.carbusiness').config(['$stateProvider',
  function($stateProvider) {
    $stateProvider.state('carbusiness example page', {
      url: '/carbusiness/example',
      templateUrl: 'carbusiness/views/index.html'
    });
  }
]);
