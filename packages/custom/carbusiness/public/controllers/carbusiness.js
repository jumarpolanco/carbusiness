'use strict';

/* jshint -W098 */
angular.module('mean.carbusiness').controller('CarbusinessController', ['$scope', 'Global', 'Carbusiness',
  function($scope, Global, Carbusiness) {
    $scope.global = Global;
    $scope.package = {
      name: 'carbusiness'
    };
  }
]);
