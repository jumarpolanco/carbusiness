var mongoose = require('mongoose');

var VehiculoSchema = new mongoose.Schema({
  tipoVehiculo: { type: mongoose.Schema.Types.ObjectId, ref: 'TipoVehiculo' },
  combustible: { type: mongoose.Schema.Types.ObjectId, ref: 'Combustible' },
  modelo: { type: mongoose.Schema.Types.ObjectId, ref: 'Modelo' },
  clasificacion: { type: mongoose.Schema.Types.ObjectId, ref: 'ClasificacionVehiculo' },
  noMotor: String,
  noChasis: String,
  noPlaca: String,
  costoDiario: Number,
  estado: String
});

mongoose.model('Vehiculo', VehiculoSchema);