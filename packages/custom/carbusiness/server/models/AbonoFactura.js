var mongoose = require('mongoose');

var AbonoFacturaSchema = new mongoose.Schema({
    factura: { type: mongoose.Schema.Types.ObjectId, ref: 'Factura' },
    fecha: {type: Date, default: Date.now},
    monto: Number
});

mongoose.model('AbonoFactura', AbonoFacturaSchema);