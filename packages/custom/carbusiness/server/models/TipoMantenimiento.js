var mongoose = require('mongoose');

var TipoMantenimientoSchema = new mongoose.Schema({
    padre: { type: mongoose.Schema.Types.ObjectId, ref: 'TipoMantenimientoSchema' },
    descripcion: String,
    prioridad: Number,
    frecuencia: Number
});

mongoose.model('TipoMantenimiento', TipoMantenimientoSchema);