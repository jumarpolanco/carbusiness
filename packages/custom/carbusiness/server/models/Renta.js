var mongoose = require('mongoose');

var RentaSchema = new mongoose.Schema({
    cliente: { type: mongoose.Schema.Types.ObjectId, ref: 'Cliente' },
    usuario: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    vehiculo: { type: mongoose.Schema.Types.ObjectId, ref: 'Vehiculo' },
    noContrato: String,
    fechaInicio: Date,
    fechaCompromiso: Date,
    deposito: Number,
    moraTiempo: Number,
    moraPago: Number
});

mongoose.model('Renta', RentaSchema);