var mongoose = require('mongoose');

var FacturaSchema = new mongoose.Schema({
    cliente: { type: mongoose.Schema.Types.ObjectId, ref: 'Cliente' },
    usuario: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    fecha: { type: Date, default: Date.now},
    total: Number,
    impuesto: Number,
    totalMora: Number,
    totalNeto: Number,
    listaDetalles: [{ type: mongoose.Schema.Types.ObjectId, ref: 'DetalleFactura' }],
    listaMora: [{type: mongoose.Schema.Types.ObjectId, ref: 'Mora'}],
    listaAbonos: [{type: mongoose.Schema.Types.ObjectId, ref: 'AbonoFactura'}],
    estado: String    
});

mongoose.model('Factura', FacturaSchema);