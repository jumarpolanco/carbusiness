var mongoose = require('mongoose');

var MoraSchema = new mongoose.Schema({
    factura: { type: mongoose.Schema.Types.ObjectId, ref: 'Factura' }
    costoDiario: Number,
    diasAtraso: Number
});

mongoose.model('Mora', MoraSchema);