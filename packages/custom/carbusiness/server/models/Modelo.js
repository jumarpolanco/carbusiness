var mongoose = require('mongoose');

var ModeloSchema = new mongoose.Schema({
  descripcion: String,
  marca: { type: mongoose.Schema.Types.ObjectId, ref: 'Marca' }
});

mongoose.model('Modelo', ModeloSchema);