var mongoose = require('mongoose');

var ClienteSchema = new mongoose.Schema({
    persona: { type: mongoose.Schema.Types.ObjectId, ref: 'Persona' },
    limiteCredito: Number,
    clasificacion: String,
    estado: String
});

mongoose.model('Cliente', ClienteSchema);


