var mongoose = require('mongoose');

var ReferenciaSchema = new mongoose.Schema({
    cliente: { type: mongoose.Schema.Types.ObjectId, ref: 'Cliente' },
    persona: { type: mongoose.Schema.Types.ObjectId, ref: 'Persona' },
    relacion: String
});

mongoose.model('Referencia', ReferenciaSchema);