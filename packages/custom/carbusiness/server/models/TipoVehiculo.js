var mongoose = require('mongoose');

var TipoVehiculoSchema = new mongoose.Schema({
  descripcion: String
});

mongoose.model('TipoVehiculo', TipoVehiculoSchema);