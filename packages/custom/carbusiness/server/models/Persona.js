var mongoose = require('mongoose');

var PersonaSchema = new mongoose.Schema({
  identificacion: { type: String, minlength: 9, required: true },
  nombres: { type: String, required: true },
  apellidos: { type: String, required: true },
  fechaNac: Date,
  nacionalidad: String,
  direccion: String,
  ciudad: String,
  pais: String,
  telefono: String,
  celular: { type: String, required: true },
  ocupacion: String  
});

mongoose.model('Persona', PersonaSchema);