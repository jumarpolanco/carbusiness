var mongoose = require('mongoose');

var MantenimientoSchema = new mongoose.Schema({
    vehiculo: { type: mongoose.Schema.Types.ObjectId, ref: 'Vehiculo' },
    tipo: { type: mongoose.Schema.Types.ObjectId, ref: 'TipoMantenimientoSchema' },
    usuario: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    fecha: { type: Date, default: Date.now},
    costo: Number
});

mongoose.model('Mantenimiento', MantenimientoSchema);